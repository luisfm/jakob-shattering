/*
 * Author: Luis Fueris Martín
 * Last updated: Jan 11, 2019
 * Comments: the supervision of this work was led by Rubén Gran and Jesús 
 * Alastruey. Please, you must compile as follows:
 * 
 */

#ifndef JAKOB_SHATTERING_H
#define JAKOB_SHATTERING_H


#define HOSTNAME_SIZE	128

/* crunch args and number of slaves' passwords */
#define ARGS_LEN		4
#define SLAVES_PWDS		0.4

/* message tags */
#define FIRST_PHASE		1
#define SECOND_PHASE	2
#define THIRD_PHASE		3

/* package received on second phase 'free\0' */
#define SECOND_ID_PKG			0
#define THIRD_ID_PKG			1
#define LAST_ID_PKG				2

#define SECOND_PKG_SIZE 		4
#define THIRD_PKG_SIZE 			5
#define LAST_PKG_SIZE 			4
#define MAX_PKG_SIZE			THIRD_PKG_SIZE
const char *messages[] = { "FREE", "FOUND", "STOP" };

#define INITINFO_SIZE 	4
#define MAX_CHARSET		128
struct init_info {
	char hash[MD5_DIGEST_LENGTH * 2 + 1];
	int pwd_len_slaves;
	int pwd_len_master;
	char charset[MAX_CHARSET];
};

#endif	
