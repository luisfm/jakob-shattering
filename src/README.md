# Versioning 

1. [Version 1.0.0](#v1.0.0)
2. [Version 1.0.1](#v1.0.1)

## v1.0.0 <a name="v1.0.0"></a>

* Generation of plain-text dictionary with [crunch](https://github.com/crunchsec/crunch) tool
* *MD5* hashing support
* Manually [OpenMP](https://www.openmp.org) parallelization with static workload distribution
* Message exchange with [OpenMPI](https://www.open-mpi.org)

### Bugs
1. On the line number 161 of ``jakob_shattering.c``

```c
160     int npwds = pow(charset_len, init_msg.pwd_len_master);
161     char *plain_pwds[npwds];
```

The program crashes when it needs to allocate 4.782.969 pointers of chars. It seems that the problem is related with node stack size. We can check it with the following command.

```bash
$ ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 128064
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 128064
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited
```
 
### Notes

1. **DONE**. Change ``pragma omp critical`` for ``pragma omp atomic write``. It reduces overhead. It does not improve the time, you can check it.
2. **TODO**. Slaves should launch a thread to receive *STOP* message their execution immediately.
3. **DONE**. Allocate ``plain_pwds`` with ``malloc()`` function to fix above bug. Please check [Version 1.0.1](#v1.0.1).


## v1.0.1 <a name="v1.0.1"></a> 

* Version 1.0.0 bug fixed. Allocate ``plain_pwds`` variable in *heap* zone.

```c
int npwds = pow(charset_len, init_msg.pwd_len_master);
char **plain_pwds = malloc(sizeof(char*) * npwds);
```

### Bugs

Please if you find any bug email me on *699623 at unizar dot es* or report me on my personal [Twitter account](https://twitter.com/lfm3773). Thanks for the collaboration!

### Notes

1. **TODO**. Slaves should launch a thread to receive *STOP* message and stop their execution immediately.
2. **TODO**. MD5 vectorization.
