/*
 * Author: Luis Fueris Martín
 * Last updated: Jan 11, 2019
 * Comments: the supervision of this work was led by Rubén Gran and Jesús 
 * Alastruey. Please, you must compile as follows:
 * $> make clean && make 
 * 
 */

#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <openssl/md5.h>
#include <omp.h>
#include <mpi.h>
#include "crunch.h" 
#include "jakob_shattering.h" 


/*
 * [debug__print_plain]: prints plain-text password arrays. This function has
 * debug purposes.
 *
 * @plainpwds: plain-text passwords array
 *
 */
static void debug__print_plainpwds(char *plain_pwds[], int npwds)
{
    int i;
    for (i = 0; i < npwds; i++) {
        printf("plain_pwds[%d] %s\n", i, plain_pwds[i]);
    }
}


/* 
 * [generate_hash]: generate a passwords hash
 *      
 * @plain_pwd: plain-text password pointer
 * @result: hashing password pointer
 *       
 */       
static void generate_hash(char *plain_pwd, char *result)
{                                                                                
    int i;
    /* 16 (MD5_DIGEST_LENGTH) * 8b (char) = 128b */                              
    unsigned char digest[MD5_DIGEST_LENGTH];                                     
    MD5((unsigned char*)plain_pwd, sizeof(char) * strlen(plain_pwd),               
                                                    (unsigned char*)&digest);    
    /* 02x means if your provided value is less than two digits then 0
        will be prepended */                                                     
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&result[i*2], "%02x", (unsigned int)digest[i]);                  
    }

    result[i*2+1] = '\0';
}


/*
 * [deallocate_mem]: deallocating memory of plain-text passwords, hashing 
 * passwords and crunch arguments arrays.
 *
 * @npwds: number of passwords (and hashes)
 * @plainpwds: plain-text passwords array pointer
 * @hashing_pwds: hashing passwords array pointer
 * @args: arguments array pointer
 *
 */
static void deallocate_mem(int npwds, char *plain_pwds[], 
                            char *hashing_pwds[], char *args[]) 
{
    int i;
    if (hashing_pwds == NULL) {
        /* master code */
        for (i = 0; i < npwds; i++) {
            free(plain_pwds[i]); 
        }

        free(plain_pwds);
    } else {
        /* slaves code */
        for (i = 0; i < npwds; i++) {
            free(plain_pwds[i]); 
            free(hashing_pwds[i]);
        }
    }

    for (i = 1; i < ARGS_LEN; i++) {
        free(args[i]);
    }
}


/*
 * [allocate_mem]: allocating memory of crunch arguments, plain-text passwords,
 * hashing passwords arrays.
 *
 * @args: arguments array pointer
 * @argv: main arguments array pointer
 * @hash: target hash password
 * @npwds: numnber of passwords (and hashes)
 * @pwd_len: supposed password length
 * @plainpwds: plain-text passwords array pointer
 * @hashing_pwds: hashing passwords array pointer
 *
 */
static void allocate_mem(char *args[], char *argv[], int npwds, int pwd_len, 
                            char *plain_pwds[], char *hashing_pwds[]) 
{
    int i;
    char pwd_len_str[MAX_CHARSET];
    sprintf(pwd_len_str, "%d", pwd_len);
    int size_pwd_len = strlen(pwd_len_str);

    /* copy from argv to args */
    args[ARGS_LEN-3] = malloc(sizeof(char) * (size_pwd_len + 1));
    args[ARGS_LEN-2] = malloc(sizeof(char) * (size_pwd_len + 1));
    args[ARGS_LEN-1] = malloc(sizeof(char) * (strlen(argv[2]) + 1));
    strncpy(args[ARGS_LEN-3], pwd_len_str, size_pwd_len + 1);
    strncpy(args[ARGS_LEN-2], pwd_len_str, size_pwd_len + 1);
    strncpy(args[ARGS_LEN-1], argv[2], strlen(argv[2]) + 1);
    
    /* there is a bottle-neck here, introduce OpenMP to speed-up for loops */
    if (hashing_pwds == NULL) {    
        /* master code */
        for (i = 0; i < npwds; i++) {
            plain_pwds[i] = malloc(sizeof(char) * (pwd_len + 1));
        }
    } else { 
        /* slaves code */
        for (i = 0; i < npwds; i++) {
            plain_pwds[i] = malloc(sizeof(char) * (pwd_len + 1));
            /* 128 bits / 4 bits = 32 hexadecimal characters */                                               
            hashing_pwds[i] = malloc(sizeof(char) * 
                                        (MD5_DIGEST_LENGTH * 2 + 1));
        }
    }
}


/*
 * [master_code]: performs the three phases of communication protocol between
 * master and slaves. This is the master function.
 *
 * @argv: initial main arguments pointer
 * @charset_len: charset length
 * @pwd_len: supposed password length
 * @nslaves: number of slaves nodes
 * @INITINFO_DATATYPE: init_info MPI datatype
 *
 */
static int master_state_machine(char host[], char *argv[], int charset_len, 
                                int pwd_len, int nslaves, 
                                MPI_Datatype INITINFO_DATATYPE)
{
    int i;
    char *args[ARGS_LEN] = {"crunch", NULL, NULL, NULL}; 
    struct init_info init_msg; 
    init_msg.pwd_len_slaves = round(pwd_len * SLAVES_PWDS);
    init_msg.pwd_len_master = pwd_len - init_msg.pwd_len_slaves;
    int npwds = pow(charset_len, init_msg.pwd_len_master); 
    char **plain_pwds = malloc(sizeof(char*) * npwds);
    char vrecv_package[SECOND_PKG_SIZE + 1]; 
    char vrecv_package_pwd[pwd_len + 1]; 
    char vsend_package[init_msg.pwd_len_master + 1];
    char vsend_package_stop[THIRD_PKG_SIZE + 1];
    char *send_package, *recv_package;
    int package_size, position;
    MPI_Status status;
    int next_prefix;
    bool stop;

    strncpy(init_msg.hash, argv[3], MD5_DIGEST_LENGTH * 2 + 1);
    strncpy(init_msg.charset, argv[2], charset_len + 1);

    /* allocate memory for master's prefixes */
    allocate_mem(args, argv, npwds, init_msg.pwd_len_master, plain_pwds, 
                                                                    NULL);
    /* FIRST PHASE: send to slaves hash, pwd_len * SLAVES_PWDS and            
        character set */                                                      
#ifdef DEBUG
    printf("[%s] Master: init_msg.pwd_len_master %d\n", 
                                            host, 
                                            init_msg.pwd_len_master);
    printf("[%s] Master: beginning FIRST PHASE\n", host);
#endif
    for (i = 1; i < nslaves; i++) {
        MPI_Send(&init_msg, 1, INITINFO_DATATYPE, i, FIRST_PHASE, 
                                                     MPI_COMM_WORLD);
    }

    /* generate plain-text dictionary */
    crunch(ARGS_LEN, args, plain_pwds);

    /* SECOND PHASE: recv free messages from slaves and send prefixes. Before
        while loop, master needs to wait free messages from all slaves 
       (firstly, they are free) */
#ifdef DEBUG
    printf("[%s] Master: beginning SECOND PHASE\n", host);
#endif

    MPI_Pack_size(MAX_PKG_SIZE + 1, MPI_CHAR, MPI_COMM_WORLD, &package_size);
    recv_package = malloc(sizeof(char) * package_size);
    
    stop = false;
    next_prefix = 0;
    while (!stop && next_prefix < npwds) {
        MPI_Recv((void *) recv_package, package_size, MPI_PACKED, 
                                        MPI_ANY_SOURCE, MPI_ANY_TAG, 
                                        MPI_COMM_WORLD, &status);    
        position = 0;
        MPI_Unpack((void *) recv_package, package_size, &position, 
        (void *) vrecv_package, MAX_PKG_SIZE + 1, MPI_CHAR, MPI_COMM_WORLD);
#ifdef DEBUG
        printf("[%s] Master: message with tag %d has been received from "
            "Slave no. %d (message: %s)\n", host, status.MPI_TAG, 
                                                status.MPI_SOURCE, 
                                                vrecv_package);
#endif
        if (strncmp(vrecv_package, 
                    messages[SECOND_ID_PKG], SECOND_PKG_SIZE) == 0) {
            MPI_Pack_size(init_msg.pwd_len_master + 1, MPI_CHAR, 
                                                        MPI_COMM_WORLD, 
                                                        &package_size);
            send_package = malloc(sizeof(char) * package_size);
            strncpy(vsend_package, plain_pwds[next_prefix], 
                                                init_msg.pwd_len_master + 1);

            position = 0;
            MPI_Pack((void *) vsend_package, init_msg.pwd_len_master + 1, 
            MPI_CHAR, (void *) send_package, package_size, &position, 
                                                            MPI_COMM_WORLD);
#ifdef DEBUG
            printf("[%s] Master: sending suffix no. %d to Slave no. %d\n", 
                                                    host, next_prefix, 
                                                    status.MPI_SOURCE);
#endif

            MPI_Send((void *) send_package, package_size, MPI_PACKED, 
                                            status.MPI_SOURCE, SECOND_PHASE, 
                                            MPI_COMM_WORLD);
            next_prefix++;
        } else if (strncmp(vrecv_package, 
                        messages[THIRD_ID_PKG], THIRD_PKG_SIZE + 1) == 0) {
            /* THIRD PHASE: recv target password and sends stop messages */
#ifdef DEBUG
            printf("[%s] Master: beginning THIRD PHASE\n", host);
#endif

            MPI_Pack_size(pwd_len + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                         &package_size);
            free(recv_package);
            recv_package = malloc(sizeof(char) * package_size);

            MPI_Recv((void *) recv_package, package_size, MPI_PACKED, 
                                            status.MPI_SOURCE, THIRD_PHASE, 
                                            MPI_COMM_WORLD, &status);    
            position = 0;
            MPI_Unpack((void *) recv_package, package_size, &position, 
                                                (void *) vrecv_package_pwd, 
                                                pwd_len + 1, MPI_CHAR, 
                                                MPI_COMM_WORLD);
#ifdef DEBUG
            printf("[%s] Master: has been received target password %s\n", 
                                                        host, 
                                                        vrecv_package_pwd);
#endif

            /* send STOP messages to slaves (THIRD PHASE) */
            MPI_Pack_size(LAST_PKG_SIZE + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                            &package_size);

            send_package = malloc(sizeof(char) * package_size);
            strncpy(vsend_package_stop, messages[LAST_ID_PKG], 
                                                        LAST_PKG_SIZE + 1);
            position = 0;
            MPI_Pack((void *) vsend_package_stop, LAST_PKG_SIZE + 1, 
                                MPI_CHAR, (void *) send_package, package_size, 
                                &position, MPI_COMM_WORLD);
#ifdef DEBUG
            printf("[%s] Master: sending %s messages to slaves\n", 
                                                            host, 
                                                            send_package);
#endif
            for (i = 1; i < nslaves; i++) {
                MPI_Send((void *) send_package, package_size, MPI_PACKED, 
                                                            i, THIRD_PHASE, 
                                                            MPI_COMM_WORLD);
            }

            stop = true;
        } else {
#ifdef DEBUG
            printf("[%s] Master: unknown message\n", host); 
#endif
        }

        /* master can receive FREE message or target password. package_size
           variable marks maximum number of elements to receive (OpenMPI 
           documentation) */
        MPI_Pack_size(MAX_PKG_SIZE + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                            &package_size);
        free(recv_package);
        recv_package = malloc(sizeof(char) * package_size);
    }

#ifdef DEBUG
    printf("[%s] Master: exit while loop\n", host); 
#endif
    /* sends STOP messages to slaves. Password has not been found */
    if (!stop)  {
        /* Maybe a slave found password but master exits loop because 
             next_suffix is greather or equal than npwds */

        /* send STOP messages to slaves */
        MPI_Pack_size(LAST_PKG_SIZE + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                        &package_size);

        send_package = malloc(sizeof(char) * package_size);
        strncpy(vsend_package_stop, messages[LAST_ID_PKG], 
                                                    LAST_PKG_SIZE + 1);
        position = 0;
        MPI_Pack((void *) vsend_package_stop, LAST_PKG_SIZE + 1, 
                            MPI_CHAR, (void *) send_package, package_size, 
                            &position, MPI_COMM_WORLD);
#ifdef DEBUG
        printf("[%s] Master: sending %s messages to slaves\n", 
                                                    host, 
                                                    send_package);
#endif
        for (i = 1; i < nslaves; i++) {
            MPI_Send((void *) send_package, package_size, MPI_PACKED, 
                                                        i, THIRD_PHASE, 
                                                        MPI_COMM_WORLD);
        }

    } else {
        printf("[%s] Master: has received the password %s\n", 
                                                        host, 
                                                        vrecv_package_pwd);
    }

    deallocate_mem(npwds, plain_pwds, NULL, args);                    
    return 0;                                                                 
}


/*
 * [slaves_code]: performs the three phases of communication protocol between
 * master and slaves. This is the slaves function.
 *
 * @rank: slave id
 * @INITINFO_DATATYPE: init_info MPI datatype
 *
 */
static int slaves_state_machine(char host[], int rank,
                                MPI_Datatype INITINFO_DATATYPE)  
{                                                                             
    char *argv[ARGS_LEN] = { NULL, NULL, NULL };
    char *args[ARGS_LEN] = {"crunch", NULL, NULL, NULL};
    char hash[MD5_DIGEST_LENGTH * 2 + 1];
    struct init_info init_msg;
    int charset_len, npwds;
    char *send_package, *recv_package;
    int package_size, position, recv_package_length;
    MPI_Status status;
    int next_suffix, nthreads_omp, i, j;
    bool found_password, stop;

    /* FIRST PHASE: recv from master hash, pwd_len * SLAVES_PWDS and            
    character set */                                                      
    MPI_Recv(&init_msg, 1, INITINFO_DATATYPE, 0, FIRST_PHASE, 
                                                MPI_COMM_WORLD, 
                                                &status);
    char package[init_msg.pwd_len_master + 1];

#ifdef DEBUG
    printf("[%s] Slave no. %d has been received init_msg.hash: %s " 
            "init_msg.pwd_len_slaves: %d init_msg.pwd_len_master: %d "
            "init_msg.charset: %s \n", host, 
                                        rank, 
                                        init_msg.hash, 
                                        init_msg.pwd_len_slaves,
                                        init_msg.pwd_len_master,
                                        init_msg.charset);
#endif

    charset_len = strlen(init_msg.charset);
    npwds = pow(strlen(init_msg.charset), init_msg.pwd_len_slaves); 
    int pwd_len = init_msg.pwd_len_master + init_msg.pwd_len_slaves;
    char *plain_pwds[npwds], *hashing_pwds[npwds]; 

    argv[2] = malloc(sizeof(char) * (charset_len + 1));
    strncpy(argv[2], init_msg.charset, charset_len + 1);
    allocate_mem(args, argv, npwds, init_msg.pwd_len_slaves, plain_pwds, 
                                                            hashing_pwds);
    /* generate plain-text dictionary */
    crunch(ARGS_LEN, args, plain_pwds);

    /* SECOND PHASE: send free message to master before while loop, the slave
        is free because it needs to receive suffix no. 1 */
    MPI_Pack_size(SECOND_PKG_SIZE, MPI_CHAR, MPI_COMM_WORLD, &package_size);
    send_package = malloc(sizeof(char) * package_size);
    strncpy(package, messages[SECOND_ID_PKG], SECOND_PKG_SIZE + 1);
#ifdef DEBUG
    printf("[%s] Slave no. %d sending FREE message to Master\n", host, rank);
#endif

    /* FREE message (SECOND PHASE) */
    position = 0;
    MPI_Pack((void *) package, SECOND_PKG_SIZE, MPI_CHAR, 
                                (void *) send_package, package_size, 
                                &position, MPI_COMM_WORLD);
    MPI_Send((void *) send_package, package_size, MPI_PACKED, 0, 
                                                    SECOND_PHASE, 
                                                    MPI_COMM_WORLD);

    /* slaves can receive suffix or STOP message, so if we allocate 
         recv_package_length chars, we allocate more characters than 
        we can receive but package_size marks maximum number of 
        elements to receive (OpenMPI documentation */
    found_password = false;
    recv_package_length = init_msg.pwd_len_master + LAST_PKG_SIZE;
    while (!stop) {
        MPI_Pack_size(recv_package_length + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                            &package_size);
        recv_package = malloc(sizeof(char) * package_size);
        MPI_Recv((void *) recv_package, package_size, MPI_PACKED, 0, 
                                                        MPI_ANY_TAG, 
                                                        MPI_COMM_WORLD, 
                                                        &status);    
        position = 0;
        MPI_Unpack((void *) recv_package, package_size, &position, 
                                                (void *) package, 
                                                recv_package_length + 1, 
                                                MPI_CHAR, 
                                                MPI_COMM_WORLD);

        if (strncmp(recv_package, 
                    messages[LAST_ID_PKG], LAST_PKG_SIZE + 1) != 0) {
#ifdef DEBUG
            printf("[%s] Slave no. %d has been received a master's prefix "
                                                    "%s\n", host, 
                                                    rank, package);
#endif
            #pragma omp parallel firstprivate(package) shared(found_password)
            {
            char password_to_hashing[pwd_len + 1];
            strncpy(password_to_hashing, package, 
                                        init_msg.pwd_len_master + 1);
            nthreads_omp = omp_get_num_threads();

            #pragma omp for
            for (next_suffix = 0; next_suffix < npwds; next_suffix++) {
                strncat(password_to_hashing, plain_pwds[next_suffix], 
                                            init_msg.pwd_len_slaves + 1);
#ifdef DEBUG
                printf("[%s] Slave no. %d (thread %d) will be hash the "
                        "following password %s\n", host, rank, 
                                                    omp_get_thread_num(), 
                                                    password_to_hashing);
#endif

                generate_hash(password_to_hashing, hashing_pwds[next_suffix]);
#ifdef DEBUG
                printf("[%s] Slave no. %d (thread %d) password %s hash: %s\n", 
                                                host, rank, 
                                                omp_get_thread_num(), 
                                                password_to_hashing, 
                                                hashing_pwds[next_suffix]);
#endif
                if (strncmp(hashing_pwds[next_suffix], 
                        init_msg.hash, MD5_DIGEST_LENGTH * 2 + 1) == 0) {

                    #pragma omp atomic write
                    found_password = true;

                    printf("[%s] Slave no. %d (thread %d) has found target "
                            "password: %s\n", host, rank, 
                                                omp_get_thread_num(), 
                                                password_to_hashing);

                    /* THIRD PHASE: send FOUND message and target password */
                    MPI_Pack_size(THIRD_PKG_SIZE + 1, MPI_CHAR, 
                                                        MPI_COMM_WORLD, 
                                                        &package_size);
                    send_package = malloc(sizeof(char) * package_size);
                    strncpy(package, messages[THIRD_ID_PKG], 
                                                        THIRD_PKG_SIZE + 1);
                    position = 0;
                    MPI_Pack((void *) package, THIRD_PKG_SIZE + 1, MPI_CHAR, 
                                                (void *) send_package, 
                                                package_size, &position, 
                                                MPI_COMM_WORLD);
#ifdef DEBUG
                    printf("[%s] Slave no. %d (thread %d) sending %s message"
                                " to Master\n", host, rank, 
                                                omp_get_thread_num(),
                                                package);
#endif
                    /* send FOUND message (THIRD PHASE) */
                    MPI_Send((void *) send_package, package_size, MPI_PACKED, 
                                                            0, THIRD_PHASE, 
                                                            MPI_COMM_WORLD);
                
                    MPI_Pack_size(pwd_len + 1, MPI_CHAR, MPI_COMM_WORLD, 
                                                            &package_size);
                    send_package = malloc(sizeof(char) * package_size);

                    position = 0;
                    MPI_Pack((void *) password_to_hashing, pwd_len + 1, 
                                            MPI_CHAR, (void *) send_package, 
                                            package_size, &position, 
                                            MPI_COMM_WORLD);
#ifdef DEBUG
                    printf("[%s] Slave no. %d (thread %d) sending target "
                                    "password to Master %s\n", host, rank, 
                                                        omp_get_thread_num(), 
                                                        send_package);
#endif
                    printf("[%s] Slave no. %d (thread %d) are sending "
                            "password %s\n", host, rank, 
                                            omp_get_thread_num(), 
                                            send_package);
                MPI_Send((void *) send_package, package_size, MPI_PACKED, 
                                                            0, THIRD_PHASE, 
                                                            MPI_COMM_WORLD);
                }    

                /* clean suffix */
                memset(&password_to_hashing[init_msg.pwd_len_master], 0, 
                                            init_msg.pwd_len_slaves + 1);
                }
            }

            if (!found_password) {
                /* FREE message (SECOND PHASE) */
                MPI_Pack_size(SECOND_PKG_SIZE, MPI_CHAR, MPI_COMM_WORLD, 
                                                            &package_size);
                send_package = malloc(sizeof(char) * package_size);
                strncpy(package, messages[SECOND_ID_PKG], SECOND_PKG_SIZE + 1);
#ifdef DEBUG
                printf("[%s] Slave no. %d sending FREE message to Master\n", 
                                                                host, rank);
#endif

                position = 0;
                MPI_Pack((void *) package, SECOND_PKG_SIZE, MPI_CHAR, 
                                    (void *) send_package, package_size, 
                                    &position, MPI_COMM_WORLD);
                MPI_Send((void *) send_package, package_size, MPI_PACKED, 0, 
                                                            SECOND_PHASE, 
                                                            MPI_COMM_WORLD);
            }

        } else {
#ifdef DEBUG
            printf("[%s] Slave no. %d has has been stopped\n", host, rank);
#endif
            stop = true;
        } 
    } /* end while */

    if (!found_password) {
        printf("[%s] Slave no. %d has not found password...\n", host, 
                                                                    rank);
    }

    deallocate_mem(npwds, plain_pwds, hashing_pwds, args);
    return 0;
}


/*
 * [initial_message]: shows program name and some explanation
 *
 */
void initial_message() 
{
    printf("\nJakob Shattering");
    printf("\n________________\n\n");
            
     printf("It is adistributed parallel cracking passwords project " 
            "programmed with OpenMP\nand OpenMPI. It uses Master-Slave "
            "design to spread load. We generate plain-text\ndictionary "
            "with crunch. Thank you mimayin at aciiid dot ath dot cx and\n"
            "bofh28 at gmail dot com.\n\n\n");
}

/*
 * [main]: generate plain-text passwords dictionary and hashing passwords
 *
 */
int main (int argc, char *argv[]) 
{
    int pwd_len, charset_len;
    int host_length, nthreads_omp, size, rank;
    char host[HOSTNAME_SIZE];
    MPI_Datatype INITINFO_DATATYPE;
    MPI_Datatype type[INITINFO_SIZE] = { MPI_CHAR, MPI_INT, MPI_INT, 
                                                            MPI_CHAR };
    MPI_Aint shift[INITINFO_SIZE];

    /* begin sanitize input */
    if (argc < 4) {
        printf("%s <password_len> <character_set> <md5_hash>\n", argv[0]);
        return -EINVAL;
    }

    pwd_len = atoi(argv[1]);
    if (pwd_len <= 0) {
        printf("<password_len> must be greather than 0\n");
        return -EINVAL;
    }

    charset_len = strlen(argv[2]);
    if (charset_len < 1) {
        printf("<charset> length must be greather than 1. If not password "
        " is charset itselt: %s\n", argv[2]);
        return -EINVAL;
    }

    if (charset_len == 1) {
        printf("Supposed passord is %d*%s. ", pwd_len, argv[2]);
        printf("You can specify more characters.\n");
        return 0;
    }

    /* MD5_DIGEST_LENGTH = 16 */
    if (strlen(argv[3]) < (MD5_DIGEST_LENGTH * 2) || 
                    strlen(argv[3]) > (MD5_DIGEST_LENGTH * 2)) {
        printf("<md5_hash> must be 32 characters long\n");
        return -EINVAL;
    }
    /* end sanitize input */

    host_length = HOSTNAME_SIZE,

    /* begin OpenMPI code */ 
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(host, &host_length);

    int block_len[INITINFO_SIZE] = { MD5_DIGEST_LENGTH * 2 + 1, 1, 
                                                    charset_len + 1 };
    shift[0] = offsetof(struct init_info, hash);
    shift[1] = offsetof(struct init_info, pwd_len_slaves);
    shift[2] = offsetof(struct init_info, pwd_len_master);
    shift[3] = offsetof(struct init_info, charset);
    MPI_Type_create_struct(INITINFO_SIZE, block_len, shift, type, 
                                                        &INITINFO_DATATYPE);
    MPI_Type_commit(&INITINFO_DATATYPE);
    if (rank == 0) {
        initial_message();
        printf("Executing Master on %s node\n", host);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        master_state_machine(host, argv, charset_len, pwd_len, size, 
                                                        INITINFO_DATATYPE);
    } else {
        printf("Executing Slave no. %d on %s node\n", rank, host);
        slaves_state_machine(host, rank, INITINFO_DATATYPE);
    }

    MPI_Type_free(&INITINFO_DATATYPE);
    MPI_Finalize();
    /* end OpenMPI code */     

    return 0;
}
