# Jakob Shattering

1. [Introduction](#Introduction)
2. [Explanation of distributed algorithm](#Algorithm)
3. [Setup environment](#Setup)
4. [Proofs of concept](#Poc)
    1. [First test](#Firsttest)
    2. [Second test](#Secondtest)
    3. [Third test](#Thirdtest)
    4. [Fourth test](#Fourhtest)
    5. [Fith test](#Fithtest)
5. [Conclusions and results](#Results)
6. [Miscellaneous](#Miscellaneous)
7. [References](#References)

**Important note**. English is not my mother language. I am so sorry if there are mistakes in my grammar. Check my personal [Twitter account](https://twitter.com/lfm3773) for feedback. 


## Introduction <a name="Introduction"></a>

[Jakob Shattering](https://es.wikipedia.org/wiki/Carl_Gustav_Jakob_Jacobi) is a distributed parallel cracking passwords project programmed with 
[OpenMP](https://www.openmp.org) (shared memory) and 
[OpenMPI v1.10.7](https://www.open-mpi.org) (distributed memory) to achieve parallel 
and distributed execution respectively. The paradigm that it follows is 
*Master-Slave*. The processor which will be used is an 
[Intel(R) Core(TM) i5-3470 CPU 3.20GHz](https://ark.intel.com/products/68316/Intel-Core-i5-3470-Processor-6M-Cache-up-to-3-60-GHz-) 
and it contains four cores and one thread per core (`lscpu`).  All code
has been implemented in C and compile with [mpicc v7.2.0](http://www.mpich.org/static/docs/v3.1.x/www1/mpicc.html). 

The general idea is to crack a signed password with a plain text password 
dictionary that is generated by **Slaves** machines on the fly. Furthermore, 
**Slaves** machines apply the signature and compare with hashing password at the same
time. It is a [hashcat](https://hashcat.net/hashcat/)-like idea 
but without heterogenous systems. Plain-text password dictionary are generated 
dynamically by [crunch](https://github.com/crunchsec/crunch) tool (thank you *mimayin at aciiid dot ath dot cx* and *bofh28 at gmail dot com*). The
main project goal to achieve is programming a parallel and distributed system to
crack passwords (not generate dictionaries, [crunch](https://github.com/crunchsec/crunch) 
already takes care of it). For simplicity, this proof of concept only signed password with *MD5* 
algorithm, in further versions we will extend with *Half MD5*, *SHA1*, 
*SHA2-256*, *SHA2-512*, *MD5 with salt*, *SHA256 with salt* *SHA-512 with salt* etc. 

The supervision of this work was led by 
[Rubén Gran](https://scholar.google.es/citations?user=Ce_38FkAAAAJ&hl=es) and
[Jesús Alastruey](http://webdiis.unizar.es/~chus/). I also have to thank [Darío Suárez](http://webdiis.unizar.es/~dario/). 

## Explanation of distributed algorithm <a name="Algorithm"></a>

In this section, we are going to explain the distributed algorithm that has been implemented. The input parameters are the following.

* Hashing *MD5* password ``4d186321c1a7f0f354b297e8914ab240``
* Supposed password length ``9``
* Character set ``abcd%``

The differents phases of distributed algorithm are now shown:

* **Master** sends target hash, character set, prefix and suffix password length. For example if password length is `9`, **Master** communicates to **Slaves** that they only must generate a suffix with `4` characters. So, **Master** generates the prefix with length `5`. Length allocation should be 60% to **Master** and 40% to **Slaves** (it depends on whether the number is even or odd). It should be noted that we need to send prefix length because **Slaves** have to wait for **Master's** prefixes.

![First phase](images/diagraram-master-slaves-first.png "First phase")

* **Slave** generates his dictionary statically. It must inform that it is *FREE* and call for **Master's** prefix dynamically. When a **Slave** receives prefix which is generated by **Master**, it builds password dictionary with the concatenation of the suffix and all its prefixes. Then, it executes *MD5* function and compare it with target hash. 
  * If target hash matches with calculated hash, **Slave** must send *FOUND* message to **Master** and stops its execution.
  * If target hash does not match with calculated hash, **Slaves** must be to continue with all passwords it is generated. When it comes to the end and has not found a matching, it must be request another suffix to **Master**
 
 
![Second phase](images/diagraram-master-slaves-second.png "Second phase")

* On the **Master's** side, it is got to worry about all **Slaves** are busy. If it receives a request that has *FREE* message, it must be generate a suffix to send. The suffixes are served to **Slaves** only when they are needed. If **Master** receives a *FOUND* message which indicates that hashing password has been cracked, it must send *STOP* message to
**Slaves** executions and end the execution.

![Third phase](images/diagraram-master-slaves-third.png "Third phase")


## Setup environment <a name="Setup"></a>

Before [Jakob Shattering](https://es.wikipedia.org/wiki/Carl_Gustav_Jakob_Jacobi) installation you must install [OpenMP](https://www.openmp.org/resources/openmp-compilers-tools/) and [OpenMPI](https://www.open-mpi.org/faq/?category=building). If [GCC](https://gcc.gnu.org) is already installed, enable [OpenMP](https://www.openmp.org) with ``-fopenmp``. 


Firstly, you need to download source code with ``git`` tool.

```bash
$ git clone git@gitlab.unizar.es:699623/distributed-parallel-cracking-paswords.git
```

To compile [Jakob Shattering](https://es.wikipedia.org/wiki/Carl_Gustav_Jakob_Jacobi), please enter in ``./distributed-parallel-cracking-paswords/src/`` directory and execute ``make``. The output should be the following.


```bash
$ make
mpicc -Iinclude/ -fopenmp   -c -o jakob_shattering.o jakob_shattering.c
mpicc -Iinclude/ -fopenmp   -c -o crunch.o crunch.c
mpicc -Iinclude/ -fopenmp -o jakob_shattering jakob_shattering.o crunch.o -lm -lpthread -lcrypto 
```

Also, if you need to view all messages between **Master** and **Slaves** you can compile source code with *debug* option.

```bash 
$ make -e DEBUG=1
mpicc -Iinclude/ -fopenmp -DDEBUG   -c -o jakob_shattering.o jakob_shattering.c
mpicc -Iinclude/ -fopenmp -DDEBUG   -c -o crunch.o crunch.c
mpicc -Iinclude/ -fopenmp -DDEBUG -o jakob_shattering jakob_shattering.o crunch.o -lm -lpthread -lcrypto
```

If you want to use [OpenMPI](https://www.open-mpi.org) library with multiple nodes, you must specify a file which it is called [hostfile](src/hostfile). According to the [documentation](https://www.open-mpi.org/faq/?category=running#mpirun-hostfile) this file specifies the hosts on which MPI processes run. An example can see below.

```
10.3.16.43 slots=2 max_slots=4
10.3.16.45 slots=2 max_slots=4
10.3.16.46 slots=2 max_slots=4
```

The `slots` field specifies the number of cores in which MPI processes will be executed. So  `max_slots` specifies the maximum number of cores in a node. In this case, nodes have four cores. Finally, you can specify number of OpenMP's threads. Please export the following environment variable.

```bash
$ export OMP_NUM_THREADS=4
```

Before setup number of threads, please you must see how many threads per core has your processor.

## Proofs of concept <a name="Poc"></a>

In the above diagrams, we have used the following input parameters:

* Hashing *MD5* password ``4d186321c1a7f0f354b297e8914ab240``
* Supposed password length ``9``
* Character set ``abcd%``. 


In character set parameter, please you should enter it as follows: ``<letters><numbers><symbols>``. There are some discrepancies when interacting with the [crunch](https://github.com/crunchsec/crunch) interface so we need to stablish a naming convention.

To launch [Jakob Shattering](https://es.wikipedia.org/wiki/Carl_Gustav_Jakob_Jacobi) execute [mpirun](https://www.open-mpi.org/doc/v1.10/man1/mpirun.1.php). Please note, ``--mca`` option indicates that MPI communications use TCP via ``br0`` interface and ``-n`` option specifies number of MPI process. 

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 -n 6 jakob_shattering 9 abcd% 4d186321c1a7f0f354b297e8914ab240

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 3 on lab002-045 node
Executing Slave no. 2 on lab002-045 node
Executing Slave no. 5 on lab002-046 node
Executing Slave no. 4 on lab002-046 node
[lab002-043] Slave no. 1 has not found password...
[lab002-046] Slave no. 5 has not found password...
[lab002-045] Slave no. 2 has not found password...
[lab002-046] Slave no. 4 has not found password...
[lab002-045] Slave no. 3 has not found password...
```

Password has been not found because the password is ``hola`` and character set does not correspond with its letters. You can found hashing password if you execute the following command.

```
$ md5 -shola
MD5 ("hola") = 4d186321c1a7f0f354b297e8914ab240
```
 
Now, we can test with right input parameters. The key is define right password length and character set.

* Hashing *MD5* password ``4d186321c1a7f0f354b297e8914ab240``
* Supposed password length ``4``
* Character set ``hlamo%``

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 -n 6 jakob_shattering 4 hlam%o 4d186321c1a7f0f354b297e8914ab240

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 5 on lab002-046 node
Executing Slave no. 3 on lab002-045 node
Executing Slave no. 4 on lab002-046 node
Executing Slave no. 2 on lab002-045 node
[lab002-043] Slave no. 1 (thread 0) has found target password: hola
[lab002-043] Slave no. 1 (thread 0) are sending password hola
[lab002-043] Master: has received the password hola
[lab002-046] Slave no. 4 has not found password...
[lab002-046] Slave no. 5 has not found password...
[lab002-045] Slave no. 3 has not found password...
[lab002-045] Slave no. 2 has not found password...
```

In above example, we can see that password has been found. In the following section we can see more examples of their use.

### First test <a name="Firsttest"></a>

* Hashing *MD5* password ``ab31caa6ef5359077abed6b118812034``
* Supposed password length ``8``
* Character set ``eysih2c``

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 -n 6 jakob_shattering 8 eysih2c ab31caa6ef5359077abed6b118812034

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 5 on lab002-046 node
Executing Slave no. 2 on lab002-045 node
Executing Slave no. 4 on lab002-046 node
Executing Slave no. 3 on lab002-045 node
[lab002-043] Slave no. 1 (thread 1) has found target password: scheisse
[lab002-043] Slave no. 1 (thread 1) are sending password scheisse
[lab002-043] Master: has received the password scheisse
[lab002-045] Slave no. 2 has not found password...
[lab002-046] Slave no. 4 has not found password...
[lab002-046] Slave no. 5 has not found password...
[lab002-045] Slave no. 3 has not found password...
```

### Second test <a name="Secondtest"></a>

* Hashing *MD5* password ``1e3950033e97287408d9b82bfba6c46c``
* Supposed password length ``9``
* Character set ``ntmoe123456``

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 -n 6 jakob_shattering 9 ntmoe123456 1e3950033e97287408d9b82bfba6c46c 

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 5 on lab002-046 node
Executing Slave no. 2 on lab002-045 node
Executing Slave no. 3 on lab002-045 node
Executing Slave no. 4 on lab002-046 node
[lab002-046] Slave no. 5 (thread 1) has found target password: 6162monet
[lab002-046] Slave no. 5 (thread 1) are sending password 6162monet
[lab002-043] Master: has received the password 6162monet
[lab002-043] Slave no. 1 has not found password...
[lab002-045] Slave no. 3 has not found password...
[lab002-045] Slave no. 2 has not found password...
[lab002-046] Slave no. 4 has not found password...

```

### Third test <a name="Thirdtest"></a>

* Hashing *MD5* password ``b2fbd92ff426554b8d7c322396c4e886``
* Supposed password length ``8``
* Character set ``ticeas196``

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 --oversubscribe -n 8 jakob_shattering 8 ticeas196 b2fbd92ff426554b8d7c322396c4e886

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 2 on lab002-043 node
Executing Slave no. 3 on lab002-045 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 4 on lab002-045 node
Executing Slave no. 6 on lab002-046 node
Executing Slave no. 7 on lab002-046 node
Executing Slave no. 5 on lab002-045 node
[lab002-046] Slave no. 7 (thread 0) has found target password: cesati91
[lab002-046] Slave no. 7 (thread 0) are sending password cesati91
[lab002-043] Master: has received the password cesati91
[lab002-043] Slave no. 2 has not found password...
[lab002-043] Slave no. 1 has not found password...
[lab002-046] Slave no. 6 has not found password...
[lab002-045] Slave no. 4 has not found password...
[lab002-045] Slave no. 5 has not found password...
[lab002-045] Slave no. 3 has not found password...
```

### Fourth test <a name="Fourhtest"></a>

* Hashing *MD5* password ``f25a2fc72690b780b2a14e140ef6a9e0 ``
* Supposed password length ``8``
* Character set ``mnveloyui=?%``


```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 --oversubscribe -n 12 jakob_shattering 8 mnveloyui=?% f25a2fc72690b780b2a14e140ef6a9e0

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 2 on lab002-043 node
Executing Slave no. 4 on lab002-045 node
Executing Slave no. 6 on lab002-045 node
Executing Slave no. 10 on lab002-046 node
Executing Slave no. 3 on lab002-043 node
Executing Slave no. 8 on lab002-046 node
Executing Slave no. 11 on lab002-046 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 7 on lab002-045 node
Executing Slave no. 5 on lab002-045 node
Executing Slave no. 9 on lab002-046 node
[lab002-045] Slave no. 7 (thread 2) has found target password: iloveyou
[lab002-045] Slave no. 7 (thread 2) are sending password iloveyou
[lab002-043] Master: has received the password iloveyou
[lab002-043] Slave no. 3 has not found password...
[lab002-043] Slave no. 2 has not found password...
[lab002-046] Slave no. 9 has not found password...
[lab002-043] Slave no. 1 has not found password...
[lab002-046] Slave no. 11 has not found password...
[lab002-045] Slave no. 6 has not found password...
[lab002-046] Slave no. 8 has not found password...
[lab002-046] Slave no. 10 has not found password...
[lab002-045] Slave no. 5 has not found password...
[lab002-045] Slave no. 4 has not found password...
```

### Fith test <a name="Fithtest"></a>

* Hashing *MD5* password ``ff64a1c43498d955147518733ac88c7c ``
* Supposed password length ``5``
* Character set ``sSahne$?``

```bash
$ mpirun --hostfile hostfile --mca btl_tcp_if_include br0 --oversubscribe -n 12 jakob_shattering 5 sSahne$? ff64a1c43498d955147518733ac88c7c    

Jakob Shattering
________________

It is adistributed parallel cracking passwords project programmed with OpenMP
and OpenMPI. It uses Master-Slave design to spread load. We generate plain-text
dictionary with crunch. Thank you mimayin at aciiid dot ath dot cx and
bofh28 at gmail dot com.


Executing Master on lab002-043 node
Executing Slave no. 2 on lab002-043 node
Executing Slave no. 8 on lab002-046 node
Executing Slave no. 4 on lab002-045 node
Executing Slave no. 6 on lab002-045 node
Executing Slave no. 10 on lab002-046 node
Executing Slave no. 3 on lab002-043 node
Executing Slave no. 7 on lab002-045 node
Executing Slave no. 1 on lab002-043 node
Executing Slave no. 5 on lab002-045 node
Executing Slave no. 9 on lab002-046 node
Executing Slave no. 11 on lab002-046 node
[lab002-046] Slave no. 8 (thread 1) has found target password: Senha
[lab002-046] Slave no. 8 (thread 1) are sending password Senha
[lab002-043] Master: has received the password Senha
[lab002-043] Slave no. 1 has not found password...
[lab002-043] Slave no. 2 has not found password...
[lab002-045] Slave no. 7 has not found password...
[lab002-045] Slave no. 6 has not found password...
[lab002-045] Slave no. 5 has not found password...
[lab002-043] Slave no. 3 has not found password...
[lab002-045] Slave no. 4 has not found password...
[lab002-046] Slave no. 11 has not found password...
[lab002-046] Slave no. 10 has not found password...
[lab002-046] Slave no. 9 has not found password...

```

## Conclusions and results <a name="Results"></a>

Now, we are going to show that our distributed algorithm is scalable to multiple nodes. The input parameters are as follows:

* Hashing *MD5* password ``5793369f738a2dc0f849f43b3328a1ee``
* Supposed password length ``11``
* Character set ``eotrc-%``


It should be noted that the number of cores is equal to number of MPI process. We launch one MPI process per core.

| Cores | Time (min) |
|:-:|:-:|
| 2 | 27,36451 |
| 4 | 9,45506 |
| 8 | 7,34457 |
| 16 | 5,16316 |
| 32 | 3,17416 |


In the following chart, we can see the relationship between the number of cores (*x-axis*) versus time (*y-axis*). It can be seen that the algorithm is scalable.

![Number of cores in relation with time](images/ncores-time.png "Number of cores in relation with time")


Now we launch one MPI process per node and this MPI process launchs four threads. On *Master* node, we only launch one thread. To do this first we need to set two environemnt variables:

* ``OMP_NUM_THREADS=4`` specifies the number of threads to use
* ``GOMP_CPU_AFFINITY="0 1 2 3"`` bind threads to specific CPUs


For further information, please you must consult the [documentation](https://gcc.gnu.org/onlinedocs/libgomp/Environment-Variables.html) of OpenMP environment variables. So the [hostfile](src/hostfile) would look like this.

```bash
10.3.16.42 slots=1 max_slots=1
10.3.16.43 slots=1 max_slots=4
```

An example of execution could be the following.

```bash
$ export OMP_NUM_THREADS=4
$ export GOMP_CPU_AFFINITY="0 1 2 3"
$ time mpirun --hostfile hostfile --mca btl_tcp_if_include br0 -n 2 -x OMP_NUM_THREADS -x GOMP_CPU_AFFINITY jakob_shattering 11 eotrc-% 5793369f738a2dc0f849f43b3328a1ee &                                                                           
```

Note that ``-x`` option. It is used to export environment variables to remote nodes. The times could be seen below.


| Threads | Time (min) |
|:-:|:-:|
| 2 | 19,12900 |
| 4 | 12,46941 |
| 8 | 6,12020 |
| 16 | 3,10483 |
| 32 | 1,36479 |

In the following chart, we can see the relationship between the number of threads (*x-axis*) versus time (*y-axis*).  

![Number of threads in relation with time](images/nthreads-time.png "Number of threads in relation with time")

If we compare above charts, we can see that in the second we get faster executions.  This is due to because we exploit threads per core that processor offer us. The target password is ``cerro-torre`` in honour of [Cerro Torre](https://en.wikipedia.org/wiki/Cerro_Torre), a mountain in Patagonia. 


## Miscellaneous <a name="Miscellaneous"></a>

If you want to view [OpenMP](https://www.openmp.org) threads and 
[OpenMPI](https://www.open-mpi.org) processes, you can execute ``top`` command with ``-H`` option to 
display individual threads. In this case we can view twelve threads because 
we execute three MPI processes on this node (four threads per MPI process).

```bash
$ top - 19:34:23 up 23 min,  2 users,  load average: 8,79, 7,93, 4,70
Threads: 344 total,   5 running, 339 sleeping,   0 stopped,   0 zombie
%Cpu(s): 97,8 us,  1,8 sy,  0,0 ni,  0,3 id,  0,0 wa,  0,0 hi,  0,0 si,  0,0 st
KiB Mem :  8046220 total,  7149868 free,   387700 used,   508652 buff/cache
KiB Swap:   519164 total,   519164 free,        0 used.  7325528 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                                                                                                                              
 2998 a699623   20   0  465864  10196   3900 R 34,6  0,1   3:58.52 jakob_shatterin                                                                                                                                                      
 3020 a699623   20   0  465864  12816   3900 R 34,2  0,2   3:53.55 jakob_shatterin                                                                                                                                                      
 3012 a699623   20   0  465864  10196   3900 S 34,2  0,1   3:54.27 jakob_shatterin                                                                                                                                                      
 3016 a699623   20   0  465864  10196   3900 S 34,2  0,1   3:53.76 jakob_shatterin                                                                                                                                                      
 3014 a699623   20   0  465864  10196   3900 S 33,2  0,1   3:53.60 jakob_shatterin                                                                                                                                                      
 2996 a699623   20   0  465864  12816   3900 S 32,9  0,2   3:57.57 jakob_shatterin                                                                                                                                                      
 3013 a699623   20   0  465864  10736   3904 S 32,9  0,1   3:53.42 jakob_shatterin                                                                                                                                                      
 3018 a699623   20   0  465864  12816   3900 R 32,6  0,2   3:53.19 jakob_shatterin                                                                                                                                                      
 3019 a699623   20   0  465864  12816   3900 S 32,6  0,2   3:53.06 jakob_shatterin                                                                                                                                                      
 2997 a699623   20   0  465864  10736   3904 R 32,6  0,1   3:58.26 jakob_shatterin                                                                                                                                                      
 3015 a699623   20   0  465864  10736   3904 S 32,6  0,1   3:53.56 jakob_shatterin                                                                                                                                                      
 3017 a699623   20   0  465864  10736   3904 S 31,6  0,1   3:53.13 jakob_shatterin                                                                                                                                                      
    1 root      20   0  191548   4528   2600 S  0,0  0,1   0:01.30 systemd                                                                                                                                                              
    2 root      20   0       0      0      0 S  0,0  0,0   0:00.00 kthreadd                                                                                                                                                             
    3 root      20   0       0      0      0 S  0,0  0,0   0:00.00 ksoftirqd/0                                                                                                                                                          
    5 root       0 -20       0      0      0 S  0,0  0,0   0:00.00 kworker/0:0H                                                                                                                                                         
    7 root      rt   0       0      0      0 S  0,0  0,0   0:00.00 migration/0                                                                                                                                                          
    8 root      20   0       0      0      0 S  0,0  0,0   0:00.00 rcu_bh            
```

## References <a name="References"></a>

1. [OpenMPI v1.10 documenation](https://www.open-mpi.org/doc/v1.10/)
2. [OpenMP Application Program Interface](https://www.openmp.org/wp-content/uploads/OpenMP4.0.0.pdf)
3. [C Reference](https://en.cppreference.com/w/c)
4. [Linux kernel coding style](https://www.kernel.org/doc/html/v4.11/process/coding-style.html#)
5. [OpenMP Environment Varibles](https://gcc.gnu.org/onlinedocs/libgomp/Environment-Variables.html)
6. [Semantic Versioning](https://semver.org)
