# Debugging crunch tool 

[Crunch](https://sourceforge.net/projects/crunch-wordlist/) is a wordlist generator created by mimayin@aciiid.ath.cx (2004) and bofh28@gmail.com (2008, 2009, 2010, 2011, 2012, 2013). There is not a code documentation or reference, so I am going to make a explanation about general structures and functions (not all). 

There are structs which define data thread, pattern information and program options. Below we can see them.

```c
303 struct thread_data {                                                           
304 	unsigned long long finalfilesize; /* total size of output */                  
305 	unsigned long long bytetotal;  /* total number of bytes so far */             
306 	unsigned long long bytecounter; /* count number of bytes in output resets to 0 */
307 	unsigned long long finallinecount; /* total size of output */                 
308 	unsigned long long linetotal; /* total number of lines so far */              
309 	unsigned long long linecounter; /* counts number of lines in output resets to 0 */
310 };                                                                            
311                                                                               
312 /* pattern info */                                                            
313 struct pinfo {                                                                
314   wchar_t *cset; /* character set pattern[i] is member of */                  
315   size_t clen;                                                                
316   int is_fixed; /* whether pattern[i] is a fixed value */                     
317   size_t start_index, end_index; /* index into cset for the start and end strings */
318   size_t duplicates;                                                          
319 };                                                                            
320                                                                               
321 /* program options */                                                         
322 struct opts_struct {                                                          
323   wchar_t *low_charset;                                                       
324   wchar_t *upp_charset;                                                       
325   wchar_t *num_charset;                                                       
326   wchar_t *sym_charset;                                                       
327   size_t clen, ulen, nlen, slen;                                              
328   wchar_t *pattern;                                                           
329   size_t plen;                                                                
330   wchar_t *literalstring;                                                     
331   wchar_t *startstring;                                                       
332   wchar_t *endstring;                                                         
333   size_t duplicates[4]; /* allowed number of duplicates for each charset */   
334                                                                               
335   size_t min, max;                                                            
336                                                                               
337   wchar_t *last_min;  /* last string of length min */                         
338   wchar_t *first_max; /* first string of length max */                        
339   wchar_t *min_string;                                                        
340   wchar_t *max_string; /* either startstring/endstring or calculated using the pattern */
341                                                                               
342   struct pinfo *pattern_info; /* information generated from pattern */        
343 };      
344 typedef struct opts_struct options_type;
```                                                   
 
They are self-explanatory but let's give an explanation about them:

- ```thread_data``` - contains information about generated data, that is, number of words created, size in bytes, output lines. 
- ```pinfo``` - contains information about introduced pattern by command line (if you entered it). This struct is implicit in *opts_struct*.
- ```opts_struct``` - contains information about data which the data entered by the user via command line. 
 
I think that the main struct is *opt_struct*. For example, if we execute [crunch](https://sourceforge.net/projects/crunch-wordlist/) like the following: 

```bash
./crunch 2 4 zf
```

We obtain the following values in ```options``` variable (it is variable defined as *options_type*).

```bash
[+] Begin vars DEBUG
*options.low_charset z
*options.upp_charset A
*options.num_charset 0
*options.sym_charset !
*options.clen 2
*options.ulen 26
*options.nlen 10
*options.slen 33
*options.pattern (null)
*options.plen 0
*options.literalstring -
*options.startsstring (null)
*options.endstring (null)
*options.duplicates[0] 18446744073709551615
*options.duplicates[1] 18446744073709551615
*options.duplicates[2] 18446744073709551615
*options.duplicates[3] 18446744073709551615
*options.min 2
*options.max 4
*options.last_min f
*options.first_max z
*options.min_string z
*options.max_string f
*options.pattern_info.cset z
*options.pattern_info.clen 2
*options.pattern_info.is_fixed 0
*options.pattern_info.start_index 0
*options.pattern_info.end_index 1
*options.pattern_info.duplicates 18446744073709551615
[+] End vars DEBUG
...
```

It is important to note that there are input and output variables. For example ```options.clen``` defines lowercase length letters, in this case is two (```zf```). Nevertheless, ```options.ulen``` defines uppercase lenght letters but we have not entered any uppercase letters (it is a default value which is not used). The same is true for ```options.nlen``` (numbers length) and ```options.slen``` (symbol length).


In ```main``` function there is a great input sanitization, furthermore it calls one of the two functions depending on ```flag``` variable. A snippet ```main```function code is the following:

```c
2329 int main(int argc, char **argv) {
		/* begin sanitize code */
		...
		/* end sanitize code */
3034   if (flag == 0) { /* chunk */                                                
3035     count_strings(&my_thread.linecounter, &my_thread.finalfilesize, options); 
3036                                                                               
3037     /* subtract already calculated data size */                               
3038     my_thread.finallinecount = my_thread.linecounter - my_thread.linetotal;   
3039     my_thread.finalfilesize -= my_thread.bytetotal;                           
3040                                                                               
3041     /* Appears that linetotal can never be nonzero there,                     
3042       so why are we subtracting it?  -jC */                                   
3043                                                                               
3044                                                                               
3045     /* if ((linecount > 0) && (finallinecount > linecount)) {                 
3046       finallinecount = linecount - linetotal;                                 
3047     */ /* TODO: must calculate finalfilesize during line calculation */       
3048                                                                               
3049     if (flag3 == 0) {                                                         
3050       if (suppress_finalsize == 0) {                                          
3051         fprintf(stderr,"Crunch will now generate the following amount of data: ");
3052         fprintf(stderr,"%llu bytes\n",my_thread.finalfilesize);               
3053         fprintf(stderr,"%llu MB\n",my_thread.finalfilesize/1048576);          
3054         fprintf(stderr,"%llu GB\n",my_thread.finalfilesize/1073741824);       
3055         fprintf(stderr,"%llu TB\n",my_thread.finalfilesize/1099511627776);    
3056         fprintf(stderr,"%llu PB\n",my_thread.finalfilesize/1125899906842624); 
3057       }                                                                       
3058       fprintf(stderr,"Crunch will now generate the following number of lines: ");
3059                                                                               
3060                                                                               
3061       /* I don't see any case where finallinecount is not set there           
3062           which would require the finalfilesize/(max+1) calc.                 
3063           I'm assuming this was a leftover from previous versions.            
3064           At any rate, finallinecount must now always be correct or else      
3065           other stuff will break.  So if something's wrong with line          
3066           count output, don't code around it here like it was. -jC */         
3067                                                                               
3068       /*                                                                      
3069       if (pattern == NULL)                                                    
3070         fprintf(stderr,"%llu \n",my_thread.finallinecount);                   
3071       else                                                                    
3072         fprintf(stderr,"%llu \n",my_thread.finalfilesize/(max+1));            
3073       */                                                                      
3074                                                                               
3075       fprintf(stderr,"%llu \n",my_thread.finallinecount);                     
3076                                                                               
3077       (void) sleep(3);                                                        
3078       if (flag4 == 1) {                                                       
3079         /* valgrind may report a memory leak here, because the progress report
3080           thread never actually terminates cleanly.  No big whoop. */         
3081         ret = pthread_create(&threads, NULL, PrintPercentage, (void *)&my_thread);
3082         if (ret != 0){                                                        
3083           fprintf(stderr,"pthread_create error is %d\n", ret);                
3084           exit(EXIT_FAILURE);                                                 
3085         }                                                                     
3086         (void) pthread_detach(threads);                                       
3087       }                                                                       
3088     }                                                                         
3089     my_thread.finalfilesize+=my_thread.bytetotal;                             
3090     my_thread.linecounter = 0;                                                
3091                                                                               
3092     chunk(min, max, startblock, options, fpath, outputfilename, compressalgo);
3093   }                                                                           
3094   else { /* permute */                                                        
```

- ```chunk``` - generates a wordlist containing repeated characters. For example, if we execute it as follows:

```bash
./crunch 2 3 abc
Crunch will now generate the following amount of data: 135 bytes
0 MB
0 GB
0 TB
0 PB
Crunch will now generate the following number of lines: 36 
aa
ab
ac
ba
bb
bc
ca
cb
cc
aaa
aab
aac
aba
abb
abc
aca
acb
acc
baa
bab
bac
bba
bbb
bbc
bca
bcb
bcc
caa
cab
cac
cba
cbb
cbc
cca
ccb
ccc
```
- ```Permute``` - generates a wordlist containing non-repeated characters, you must use ```-p``` option. 

```bash
./crunch 2 3 -p abc
Crunch will now generate approximately the following amount of data: 24 bytes
0 MB
0 GB
0 TB
0 PB
Crunch will now generate the following number of lines: 6 
abc
acb
bac
bca
cab
cba
```

In this project, we just need ```chunk``` function to generate a password dictionary. Please, check the following directory [./distributed-parallel-cracking-paswords/src/](https://gitlab.unizar.es/699623/distributed-parallel-cracking-paswords/tree/master/src). 
